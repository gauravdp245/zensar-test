from django.contrib import admin

# Register your models here.
from .models import Routing

admin.site.register(Routing)
