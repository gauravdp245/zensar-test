# Generated by Django 3.0.8 on 2020-07-21 08:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apitest', '0003_auto_20200721_0759'),
    ]

    operations = [
        migrations.AlterField(
            model_name='routing',
            name='hostname',
            field=models.CharField(max_length=14, unique=True),
        ),
        migrations.AlterField(
            model_name='routing',
            name='loopbackid',
            field=models.GenericIPAddressField(unique=True),
        ),
    ]
