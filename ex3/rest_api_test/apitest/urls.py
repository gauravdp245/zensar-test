from django.urls import path
from . import views

#url pattern for CURD operations
urlpatterns = [
    path('getdata/', views.RouteData.as_view(), name='load data'),
    path('getdata/<int:sap_id>', views.RouteData.as_view(), name='load data'),
    path('update/', views.RouteData.as_view(), name='updata data'),
    path('delete/', views.RouteData.as_view(), name='delete data'),
    path('add/', views.RouteData.as_view(), name='add data'),
    path('ipsearch/', views.CustomSearch.as_view(), name='custom data'),

]
