from django.http import JsonResponse
from .serializers import RoutingSerializer
from rest_framework import status
from rest_framework.decorators import (
    api_view,
    permission_classes,
    renderer_classes,
)
from rest_framework.views import APIView
from .models import Routing
from marshmallow import ValidationError
from django.db import IntegrityError, transaction
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q
from django.conf import settings
import json


class RouteData(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        #get records
        try:
            q_objects = Q()
            if kwargs.get('sap_id', None):
                q_objects &= Q(sapid=kwargs['sap_id'])
            queryset = Routing.objects.filter(q_objects)
            serializer = RoutingSerializer(queryset, many=True)
            return JsonResponse(serializer.data, safe=False, status=200)
        except Routing.DoesNotExist as err:
            return JsonResponse('Record not present.', safe=False, status=400)

    def put(self, request, *args, **kwargs):
        #update record
        try:
            data = request.data
            obj = Routing.objects.get(ip=data['ip'])
            data.pop('ip')
            if 'hostname' in data:
                obj.hostname = data['hostname']
            if 'sapid' in data:
                obj.sapid = data['sapid']
            if 'loopbackid' in data:
                obj.loopbackid = data['loopbackid']
            if 'mac_address' in data:
                obj.mac_address = data['mac_address']
            obj.save()
            return JsonResponse('Record updated successfully.', safe=False, status=status.HTTP_200_OK)

        except Routing.DoesNotExist as err:
            return JsonResponse('Record not present.', safe=False, status=400)

    def delete(self, request, *args, **kwargs):
        #delete record
        try:
            data = request.data
            obj = Routing.objects.filter(ip=data['ip']).delete()

            return JsonResponse('Record Deleted .', safe=False, status=status.HTTP_200_OK)

        except Routing.DoesNotExist as err:
            return JsonResponse('Record not present.', safe=False, status=400)


    def post(self, request, *args, **kwargs):
        #Add record
        data = request.data
        serializer = RoutingSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse('Record added .', safe=False, status=status.HTTP_200_OK)

        return JsonResponse('Record not added.', safe=False, status=400)


class CustomSearch(APIView):
    #ip range search
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        data = request.data
        if 'from_ip' not in data or 'to_ip' not in data:
            return JsonResponse('"from_ip" and "to_ip" are mandatory.', safe=False, status=400)

        queryset = Routing.objects.filter(ip__range=[data['from_ip'], data['to_ip']])
        serializer = RoutingSerializer(queryset, many=True)

        return JsonResponse(serializer.data, safe=False, status=200)


