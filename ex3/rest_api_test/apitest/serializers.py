
from rest_framework import serializers
from .models import Routing


class RoutingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Routing
        fields = ['id', 'hostname', 'sapid', 'loopbackid', 'mac_address', 'ip']

