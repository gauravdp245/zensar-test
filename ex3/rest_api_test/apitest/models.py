from django.db import models

class Routing(models.Model):

    id = models.AutoField(primary_key=True)
    hostname = models.CharField(max_length=100, unique=True)
    sapid = models.IntegerField()
    mac_address = models.CharField(max_length=100)
    loopbackid = models.GenericIPAddressField(unique=True)
    ip = models.GenericIPAddressField()

    class Meta:
        db_table = 'routing'

