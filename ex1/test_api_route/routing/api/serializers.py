from rest_framework import serializers
from .models import APIRouterDetails

class APIRouterDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = APIRouterDetails
        fields = ['id', 'hostname', 'sapid', 'loopbackid', 'mac_address']
