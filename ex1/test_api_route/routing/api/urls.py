from django.urls import path

from . import views

urlpatterns = [
    path('fetch_router/', views.APIRouterDetail.as_view()),
    path('get_router/<int:router_id>/', views.APIRouterData.as_view()),
    path('fetch_router/', views.APIRouterDetail.as_view()),
    path('add/', views.AddData.as_view()),
    path('update/<int:router_id>', views.APIRouterData.as_view()),
    path('delete/<int:router_id>', views.APIRouterDetail.as_view()),
]

