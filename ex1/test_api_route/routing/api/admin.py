from django.contrib import admin

# Register your models here.
from .models import APIRouterDetails

admin.site.register(APIRouterDetails)
