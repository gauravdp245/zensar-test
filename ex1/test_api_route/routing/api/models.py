from django.db import models

# Create your models here.

class APIRouterDetails(models.Model):
    class Meta:
        db_table = 'api_router_details'

    id = models.AutoField(primary_key=True)
    hostname = models.CharField(max_length=200)
    sapid = models.CharField(max_length=200)
    loopbackid = models.CharField(max_length=200)
    mac_address = models.CharField(max_length=100)
    active = models.BooleanField(default=True)
