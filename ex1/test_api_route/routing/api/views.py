from django.shortcuts import render
from django.http import HttpResponse
import json
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from .models import APIRouterDetails
from .serializers import APIRouterDetailsSerializer
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework import status
from rest_framework.permissions import IsAuthenticated


class APIRouterDetail(APIView):
    # permission_classes = (IsAuthenticated,)
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'details.html'

    def get(self, request, *args, **kwargs):
        queryset = APIRouterDetails.objects.filter(active=True)
        return Response({'router_data': queryset})

    def delete(self, request, *args, **kwargs):
        data = APIRouterDetails.objects.get(id=kwargs['router_id'])
        data.active=False
        data.save()
        return Response({'message': 'Data deleted successfully'}, status=status.HTTP_204_NO_CONTENT)

class APIRouterData(APIView):
    # permission_classes = (IsAuthenticated,)
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'update.html'

    def get(self, request, *args, **kwargs):
        queryset = APIRouterDetails.objects.get(id=kwargs['router_id'])
        return Response({'data' : queryset})

    def put(self, request, *args, **kwargs):
        obj = APIRouterDetails.objects.get(id=kwargs['router_id'])
        data = json.loads(request.body)
        serializer = APIRouterDetailsSerializer(obj, data=data)
        if serializer.is_valid():
            serializer.save()
            print('record saved')
            return Response({'message' : 'Record updated successfully'}, status=status.HTTP_200_OK)
        return Response({'message' : 'Record cannot be updated'})

class AddData(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'add.html'

    def get(self, request):
        return Response({'message' : 'Add new data'})

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        serializer = APIRouterDetailsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message' : 'Record inserted successfully'}, status=status.HTTP_200_OK)
        return Response({'message' : 'Record cannot be added'})

