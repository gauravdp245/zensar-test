import random
import string
from ipaddress import IPv4Address

count = 1

# Taking Hexa Decimal No
def randomMAC():
    mac = [0x00, 0x16, 0x3e,
           random.randint(0x00, 0x7f),
           random.randint(0x00, 0xff),
           random.randint(0x00, 0xff)]
    return ':'.join(map(lambda x: "%02x" % x, mac))


def random_ip_address():
    return str(IPv4Address(random.getrandbits(32)))


def random_hostname():
    N = random.randint(3, 14)
    res = ''.join(random.choices(string.ascii_uppercase +
                                 string.digits, k=N))
    return str(res)


for i in range(10):
    print('SAP_ID --- ', count)
    print('MAC_ID --- ', randomMAC())
    print('IP_ADD', random_ip_address())
    print('HOST_NAME', random_hostname())
    count+=1
