import getpass
import telnetlib

HOST = "host_name"
user = input("Enter your remote account: ")
password = getpass.getpass()

tn = telnetlib.Telnet(HOST)

tn.read_until("login: ")
tn.write(user + "\n")
if password:
    tn.read_until("Password: ")
    tn.write(password + "\n")

tn.write(b"ls\n")
tn.write(b"exit\n")

print(tn.read_all())
