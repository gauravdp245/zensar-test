from paramiko import SSHClient
from scp import SCPClient

ssh = SSHClient()
ssh.load_system_host_keys()
ssh.connect('user@server:path')

with SCPClient(ssh.get_transport()) as scp:
    # Copy my_file.txt to the server
    scp.put('my_file.txt', 'my_file.txt')
