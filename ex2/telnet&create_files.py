import getpass
import telnetlib
import os
import zipfile

HOST = "localhost"
user = input("Enter your remote account: ")
password = getpass.getpass()

tn = telnetlib.Telnet(HOST)

tn.read_until("login: ")
tn.write(user + "\n")
if password:
    tn.read_until("Password: ")
    tn.write(password + "\n")

tn.write(b"ls\n")
tn.write(b"exit\n")

# path of the current script
path = '../path'#TELNET PATH

n = int(input("Enter the No of file"))
# Creates a new file
for i in range n:
    with open(f'file_{i}.txt', 'w') as fp:
        pass
        # To write data to new file uncomment
        # this fp.write("New file created")

# check created creating
dir_list = os.listdir(path)
print("List of directories and files after creation:", dir_list)
print(tn.read_all().decode('ascii'))


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

zipf = zipfile.ZipFile('Python.zip', 'w', zipfile.ZIP_DEFLATED)
zipdir('tmp/', zipf)
zipf.close()
