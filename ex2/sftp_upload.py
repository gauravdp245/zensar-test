
import pysftp

with pysftp.Connection('hostname', username='me', password='secret') as sftp:

    # temporarily chdir to local_data
    with sftp.cd('/local_data'):

        # upload file to server/pycode on remote
        sftp.put('/server/filename')
