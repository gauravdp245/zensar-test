import ftplib

session = ftplib.FTP('server.address.com','username','password')
# file to send
file = open('kitten.jpg','rb')

# send the file
session.storbinary('STOR kitten.jpg', file)

#close connection
file.close()
session.quit()
