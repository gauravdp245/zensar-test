
CREATE TABLE emp_data (
       name text,
       age integer,
       designation text,
       salary integer
);


INSERT INTO emp_data VALUES(
  ('emp1', 24, 'software_engg', 30000),
  ('emp2', 35, 'sr.software_engg', 40000),
  ('emp3', 39, 'sr.software_engg', 50000),
  ('emp4', 20, 'intern', 15000),
  ('emp5', 25, 'software_engg', 28000),
  ('emp6', 31, 'HR', 35000),
  ('emp7', 22, 'intern', 1500),
  ('emp8', 27, 'software_engg', 30000),
  ('emp9', 21, 'jr.software_engg', 27000),
  ('emp10', 50, 'Manager', 1000000),
 );



CREATE or REPLACE VIEW emp_view AS
SELECT name, designation
FROM emp_data
WHERE age > 25;


